package com.example.springSecurityAuthentication_2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringSecurityAuthentication2Application {
@GetMapping("/message")
public String message(){
	return "hello";
}
	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityAuthentication2Application.class, args);
	}

}
