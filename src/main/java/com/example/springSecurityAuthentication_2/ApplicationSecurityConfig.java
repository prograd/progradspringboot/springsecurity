package com.example.springSecurityAuthentication_2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig {
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
        //.authorize request is used for authorizing every request
        //.anyrequest is used for authorizing any users request
        //.httbasic is used for to check whether it is httpbasic or formbased authentication

        http
                .authorizeHttpRequests((authz)->authz
                        .anyRequest().authenticated()
                )
                .httpBasic(withDefaults());

        return http.build();
    }

    @Bean
    public InMemoryUserDetailsManager userDetailsService(){
        //UserDeatils is a class which is present in spring security dependency
       //used for creating users
        UserDetails user= User.withDefaultPasswordEncoder()
                .username("user")
                .password("password")
                .roles("USER")
                .build();
        UserDetails use2= User.withDefaultPasswordEncoder()
                .username("user2")
                .password("password2")
                .roles("MANAGER")
                .build();
        UserDetails use3= User.withDefaultPasswordEncoder()
                .username("user3")
                .password("password3")
                .roles("USER")
                .build();
        return new InMemoryUserDetailsManager(user,use2,use3);
    }
}
